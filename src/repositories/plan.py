from sqlalchemy import or_
from sqlalchemy.orm import Session

from schemas.other import FilterSchema
from sql_app.models.Plan import Plan as plan_model


def get_plan_by_channel_id(db: Session, channel_id: int, filters: FilterSchema):
    records = db.query(plan_model).filter(or_(plan_model.channel_id == channel_id, plan_model.type == 70))
    return records.limit(filters.limit).offset(filters.offset).all()