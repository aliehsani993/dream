import hashlib
from sqlalchemy.orm import Session, selectinload

# from schema.channel import ChannelInDB, ChannelOutput
from schema.user import UserInDB
from schemas.channel import ChannelInDbSchema
from sql_app.models import channel as channel_model
from sql_app.models import category as cate


def create_channel(db: Session, channel: ChannelInDbSchema, user_id):
    db_channel = channel_model.Channel(
        avatar=channel.avatar,
        cover=channel.cover,
        social_type=channel.social_type,
        category_id=channel.category_id,
        follower_count=channel.follower_count,
        title=channel.title,
        user_id=user_id
    )

    db.add(db_channel)
    db.commit()
    db.refresh(db_channel)

    return db_channel


def find_channel_by_user_id(db: Session, user_id: int):
    return db.query(channel_model.Channel).filter(channel_model.Channel.user_id == user_id).first()


def find_channel_by_id(db: Session, id: int):
    return db.query(channel_model.Channel).filter(channel_model.Channel.id == id).first()
#.options(selectinload(channel_model.Channel.category))