import hashlib
from sqlalchemy.orm import Session
from schemas.user import UserInDbSchema
from sql_app.models import user as user_model


def find_user_by_mobile(db: Session, mobile: str):
    return db.query(user_model.User).filter(user_model.User.mobile == mobile).first()


def create_user(db: Session, user: UserInDbSchema):
    pass_salt = user.password + 'dream'
    hashed_plain_password = hashlib.sha256(pass_salt.encode()).hexdigest()

    db_user = user_model.User(password=hashed_plain_password, mobile=user.mobile, name=user.name)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user
