from sqlalchemy import or_, and_
from sqlalchemy.orm import Session
from schemas.order import OrderInDbSchema
from schemas.other import FilterSchema
from sql_app.models.order import Order as order_model


def create_order(db: Session, order_input: OrderInDbSchema):
    db_order = order_model(**order_input.dict())

    db.add(db_order)
    db.commit()
    db.refresh(db_order)

    return db_order


def get_orders_by_user_id(db: Session, user_id: int, filters: FilterSchema):
    records = db.query(order_model).filter(and_(order_model.user_id == user_id, order_model.applicant_status is not None))
    return records.limit(filters.limit).offset(filters.offset).all()


def find_order_by_id(db: Session, id: int):
    return db.query(order_model).filter(order_model.id == id).first()


def find_order_by_channel_id(db: Session, channel_id: int, filters: FilterSchema):
    return db.query(order_model).filter(order_model.channel_id == channel_id).all()
