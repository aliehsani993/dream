from sqlalchemy.orm import Session

from schemas.tag import TagRelationInDbSchema
from sql_app.models import tag_relation as tag_relation_model


def create_tag_relation(db: Session, tag: TagRelationInDbSchema):
    db_tag_relation = tag_relation_model.TagRelation(**tag.dict())
    db.add(db_tag_relation)
    db.commit()
    db.refresh(db_tag_relation)

    return db_tag_relation
