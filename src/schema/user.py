from typing import Optional
from pydantic import BaseModel


class User(BaseModel):
    name: str = None
    mobile: str
    email: Optional[str] = None

    class Config:
        orm_mode = True


class UserInDB(User):
    password: str

