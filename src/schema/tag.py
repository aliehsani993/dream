from pydantic import BaseModel


class Tag(BaseModel):
    id: int
    title: str = None
    type: int = None


class TagOutput(Tag):
    pass

    class Config:
        orm_mode = True







class TagRelation(BaseModel):
    entity_type_id: int = None
    entity_id: int = None
    tag_id: int

class TagRelationInDB(TagRelation):
    pass