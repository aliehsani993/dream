from pydantic import BaseModel



class File(BaseModel):
    id: int
    path: str = None
    title: str =None
    type: int =None
    format: str =None
    link: str =None


class FileOutput(File):
    id: int
    url: str

    class Config:
        orm_mode = True


