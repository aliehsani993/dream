from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base
from sql_app.models.category import Category
from sql_app.models.file import File
from sql_app.models.user import User


class Channel(Base):
    __tablename__ = "channels"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer,  ForeignKey("users.id"))
    link = Column(String, nullable=True)
    title = Column(String, nullable=True)
    avatar = Column(Integer, ForeignKey("files.id"))
    cover = Column(Integer, ForeignKey("files.id"))
    social_type = Column(Integer, nullable=True)
    category_id = Column(Integer, ForeignKey("categories.id"))
    follower_count = Column(Integer, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.now())
    updated_at = Column(TIMESTAMP, default=func.now(), onupdate=func.now())
    deleted_at = Column(TIMESTAMP, nullable=True)

    category_obj = relationship(Category, foreign_keys=[category_id])
    avatar_obj = relationship(File, foreign_keys=[avatar])
    cover_obj = relationship(File, foreign_keys=[cover])
    owner = relationship(User, foreign_keys=[user_id])

