from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(None, nullable=True)
    mobile = Column(None, nullable=True)
    name = Column(String)
    password = Column(String)
    created_at = Column(TIMESTAMP, default=datetime.now())
    updated_at = Column(TIMESTAMP, default=func.now(), onupdate=func.now())
