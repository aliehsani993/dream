from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base


class OrderAttachment(Base):
    __tablename__ = "order_attachments"

    id = Column(Integer, primary_key=True, index=True)
    order_id = Column(Integer,ForeignKey("orders.id"))
    text = Column(String, nullable=True)
    description = Column(String, nullable=True)
    photo = Column(Integer, nullable=True)
    video = Column(Integer, nullable=True)

