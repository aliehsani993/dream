from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP, Time
from sqlalchemy.orm import relationship

from sql_app.database import Base
from sql_app.models.channel import Channel


class Plan(Base):
    __tablename__ = "plans"

    id = Column(Integer, primary_key=True, index=True)
    channel_id = Column(Integer,  ForeignKey("channels.id"))
    plan_type = Column(Integer, nullable=True)
    type = Column(Integer, nullable=True)
    start_time = Column(Time, nullable=True)
    duration = Column(Integer, nullable=True)
    price = Column(Integer, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.now())
    updated_at = Column(TIMESTAMP, default=func.now(), onupdate=func.now())
    deleted_at = Column(TIMESTAMP, nullable=True)

    channel_obj = relationship(Channel, foreign_keys=[channel_id])

