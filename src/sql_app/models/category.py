from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base



class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=True)

    # channel = relationship("Channel")