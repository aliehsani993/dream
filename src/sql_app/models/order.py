from datetime import datetime

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base
from sql_app.models.Plan import Plan
from sql_app.models.channel import Channel
from sql_app.models.order_attachment import OrderAttachment
from sql_app.models.user import User


class Order(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    channel_id = Column(Integer, ForeignKey("channels.id"))
    plan_id = Column(Integer, ForeignKey("plans.id"))
    quantity = Column(Integer, nullable=True)
    amount = Column(Integer, nullable=True)
    advertiser_status = Column(Integer, nullable=True)
    applicant_status = Column(Integer, nullable=True)
    paid_at = Column(TIMESTAMP, nullable=True)

    settlement_status = Column(Integer, nullable=True)
    settlement_receipt = Column(String, nullable=True)
    settlement_paid_at = Column(TIMESTAMP, nullable=True)
    settlement_card_number = Column(String, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.now())
    updated_at = Column(TIMESTAMP, default=func.now(), onupdate=func.now())
    deleted_at = Column(TIMESTAMP, nullable=True)

    channel_obj = relationship(Channel, foreign_keys=[channel_id])
    plan_obj = relationship(Plan, foreign_keys=[plan_id])
    user_obj = relationship(User, foreign_keys=[user_id])

    order_attachment_obj = relationship(OrderAttachment, foreign_keys=[OrderAttachment.order_id], uselist=False)
    #viewonly=True