from datetime import datetime

from sqlalchemy import  Column, ForeignKey, Integer, String, Date, DateTime, func, TIMESTAMP
from sqlalchemy.orm import relationship

from sql_app.database import Base


class TagRelation(Base):
    __tablename__ = "tag_relations"

    id = Column(Integer, primary_key=True, index=True)
    entity_id = Column(Integer, nullable=True)
    entity_type_id = Column(Integer, nullable=True)
    tag_id = Column(Integer, nullable=True)
    created_at = Column(TIMESTAMP, default=datetime.now())
    updated_at = Column(TIMESTAMP, default=func.now(), onupdate=func.now())
    deleted_at = Column(TIMESTAMP)
