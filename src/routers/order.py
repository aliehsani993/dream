from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies.auth import get_current_user
from dependencies.db import get_db
from dependencies.user import get_current_user_channel
from infra.enum.advertiser_status_enum import AdvertiserStatusEnum
from infra.enum.entity_type_enum import EntityTypeEnum
from repositories import plan as plan_repo
from repositories import order as order_repo
from schemas.Plane import PlanOutSchema
from schemas.order import OrderOutSchema, OrderInDbSchema, OrderInFormSchema, OrderOutWithRelationSchema
from schemas.other import FilterSchema

router = APIRouter(
    prefix="/order",
    tags=["order"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)


# response_model=OrderOutSchema
@router.post("/create/{plan_id}", response_model=OrderOutSchema)
def create_order(plan_id: int, order_input: OrderInFormSchema, db: Session = Depends(get_db),
                 current_user: get_current_user = Depends()):
    order_in_db = OrderInDbSchema(
        user_id=current_user.id,
        channel_id=order_input.channel_id,
        plan_id=plan_id,
        advertiser_status=32,
        applicant_status=34,
        amount=order_input.amount,
        quantity=order_input.quantity
    )

    return order_repo.create_order(db, order_in_db)

    # return AdvertiserStatusEnum(50).name
    # return AdvertiserStatusEnum.CANCEL.value


@router.post("/my_orders", response_model=List[OrderOutSchema])
def read_my_orders(filters: FilterSchema ,db: Session = Depends(get_db), current_user: get_current_user = Depends()):
    records = order_repo.get_orders_by_user_id(db, current_user.id, filters)
    if not records:
        return []
    return records

@router.post("/{order_id}",response_model=OrderOutWithRelationSchema)
def read_my_orders(order_id: int ,db: Session = Depends(get_db)):
    return order_repo.find_order_by_id(db, order_id)


@router.post("/advertiser/my_orders", response_model=List[OrderOutWithRelationSchema])
def read_channel_orders( filters: FilterSchema, db: Session = Depends(get_db), channel: get_current_user_channel = Depends()):
    if not channel:
        return []
    return order_repo.find_order_by_channel_id(db, channel.id, filters)
