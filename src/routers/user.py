from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from dependencies.auth import get_current_user
from dependencies.db import get_db
from repositories import user as user_repo
from schemas.user import UserOutSchema, UserInDbSchema

router = APIRouter(
    prefix="/user",
    tags=["user"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)


@router.post("/create/", response_model=UserOutSchema)
def create_user(user: UserInDbSchema, db: Session = Depends(get_db)):
    db_user = user_repo.find_user_by_mobile(db, mobile=user.mobile)
    if db_user:
        raise HTTPException(status_code=400, detail="mobile already registered")
    return user_repo.create_user(db, user)


@router.get("/me/", response_model=UserOutSchema)
def read_current_user(current_user: get_current_user = Depends(get_current_user)):
    return UserOutSchema(**current_user.__dict__)
