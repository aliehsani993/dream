from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies.auth import get_current_user
from dependencies.db import get_db
from infra.enum.entity_type_enum import EntityTypeEnum
from repositories import plan as plan_repo
from schemas.Plane import PlanOutSchema
from schemas.other import FilterSchema

router = APIRouter(
    prefix="/plan",
    tags=["plan"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)


@router.post("/{channel_id}", response_model=List[PlanOutSchema])
def read_plans(channel_id: int, filters: FilterSchema , db: Session = Depends(get_db)):
    return plan_repo.get_plan_by_channel_id(db, channel_id, filters)
