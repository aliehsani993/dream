from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from dependencies.auth import get_current_user
from dependencies.db import get_db
from infra.enum.entity_type_enum import EntityTypeEnum
from repositories import channel as channel_repo
from schemas.channel import ChannelOutSchema, ChannelInDbSchema
from schemas.tag import TagRelationInDbSchema
from repositories import tag as tag_repo

router = APIRouter(
    prefix="/channel",
    tags=["channel"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)


@router.post("/create/", response_model=ChannelOutSchema)
def create_channel(channel_input: ChannelInDbSchema, db: Session = Depends(get_db), current_user: get_current_user = Depends()):
    if channel_repo.find_channel_by_user_id(db, current_user.id):
        raise HTTPException(status_code=400, detail="you already created channel")

    channel = channel_repo.create_channel(db, channel_input, current_user.id)

    if channel_input.tags:
        for tag_id in channel_input.tags:
            tag_relation_data = TagRelationInDbSchema(tag_id=tag_id, entity_type_id=EntityTypeEnum.CHANNEL.value,
                                                      entity_id=channel.id)
            tag_repo.create_tag_relation(db, tag_relation_data)

    # todo:: attach files(avatar and cover)

    return channel


@router.get("/{channel_id}", response_model=ChannelOutSchema)
def get_channel(channel_id: int, db: Session = Depends(get_db)):
    return channel_repo.find_channel_by_id(db, channel_id)
