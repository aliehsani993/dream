import hashlib
from datetime import datetime, timedelta
from typing import Optional
from fastapi import APIRouter, Depends, HTTPException, status
from jose import jwt
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from dependencies.db import get_db
from repositories import user as user_repo
from schemas.token import Token
from configs import auth
from schemas.user import UserInFormSchema

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
    dependencies=[],
    responses={404: {"description": "Not found"}},
)

@router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = authenticate_user(form_data.username, form_data.password, db)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect mobile or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    user = UserInFormSchema(**user.__dict__)

    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.mobile}, expires_delta=access_token_expires
    )

    return {"access_token": access_token, "token_type": "bearer"}


def authenticate_user(mobile: str, password: str, db):
    user_object = user_repo.find_user_by_mobile(db, mobile=mobile)
    if not user_object:
        return False
    if not verify_password(password, user_object.password):
        return False
    return user_object


def verify_password(plain_password, hashed_password):
    pass_salt = plain_password + 'dream'
    hashed_plain_password = hashlib.sha256(pass_salt.encode()).hexdigest()

    if hashed_plain_password == hashed_password:
        return True
    else:
        return False


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, auth.SECRET_KEY, algorithm=auth.ALGORITHM)

    return encoded_jwt
