from pydantic import BaseModel

from schemas.channel import ChannelOutSchema


class PlanSchema(BaseModel):
    stat_time: str = None
    duration: int = None
    price: int = None


class PlanOutSchema(PlanSchema):
    id: int
    channel_obj: ChannelOutSchema = None
    plan_type: int = None
    type: int = 3

    class Config:
        orm_mode = True



class PlanInDbSchema(PlanSchema):
    id: int
    path: str = None
    title: str = None
    type: int = None
    format: str = None
    link: str = None
    plan_type: int
    type: int

    class Config:
        orm_mode = True


class PlanInFormSchema(PlanSchema):
    id: int
    path: str = None
    title: str = None
    type: int = None
    format: str = None
    link: str = None
    plan_type: int
    type: int

    class Config:
        orm_mode = True
