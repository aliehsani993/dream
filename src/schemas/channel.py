from typing import List, Optional

from pydantic import BaseModel

# Create the Pydantic models

from schemas.category import CategoryOutSchema
from schemas.file import FileOutSchema
from schemas.user import UserOutSchema


class ChannelSchema(BaseModel):
    title: str = None
    link: str = None


class ChannelOutSchema(ChannelSchema):
    category_obj: CategoryOutSchema = None
    avatar_obj: FileOutSchema = None
    cover_obj: FileOutSchema = None
    owner: UserOutSchema = None

    class Config:
        orm_mode = True


class ChannelInDbSchema(ChannelSchema):
    avatar: int = None
    cover: int = None
    social_type: int = None
    category_id: int = None
    follower_count: int = None
    tags: List[int] = []


class ChannelInFromSchema(ChannelSchema):
    pass
