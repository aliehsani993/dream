from datetime import datetime
from pydantic import BaseModel, validator

from infra.enum.advertiser_status_enum import AdvertiserStatusEnum
from infra.enum.applicant_status_enum import ApplicantStatusEnum
from infra.enum.settlement_status_enum import SettlementStatusEnum
from schemas.Plane import PlanOutSchema
from schemas.channel import ChannelOutSchema
from schemas.order_attachment import orderAttachmentOutSchema, orderAttachmentSchema


class OrderSchema(BaseModel):
    quantity: str = None
    amount: str = None
    settlement_status : int = None

    @validator('settlement_status')
    def settlement_status_validate(cls, settlement_status):
        return SettlementStatusEnum(settlement_status).name


    settlement_receipt : str = None
    settlement_paid_at: str = None
    settlement_card_number: str = None


class OrderOutSchema(OrderSchema):
    id: int
    paid_at: str = None
    advertiser_status: int = None
    applicant_status: int = None

    @validator('applicant_status' )
    def applicant_status_validate(cls, applicant_status):
        return ApplicantStatusEnum(applicant_status).name

    @validator('advertiser_status' )
    def advertiser_status_validate(cls, advertiser_status):
        return AdvertiserStatusEnum(advertiser_status).name

    class Config:
        orm_mode = True
        validate_assignment = True


class OrderOutWithRelationSchema(OrderOutSchema):
    channel_obj: ChannelOutSchema = None
    plan_obj: PlanOutSchema = None
    order_attachment_obj : orderAttachmentSchema = None

    class Config:
        orm_mode = True


class OrderInDbSchema(OrderSchema):
    user_id: int = None
    channel_id: int = None
    plan_id: int = None
    advertiser_status: int = None
    applicant_status: int = None


class OrderInFormSchema(OrderSchema):
    channel_id: int = None
