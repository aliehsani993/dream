from pydantic import BaseModel

from schemas.channel import ChannelOutSchema


class FilterSchema(BaseModel):
    limit: int = 3
    offset: int = 0
