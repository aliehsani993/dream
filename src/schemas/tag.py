from pydantic import BaseModel


class TagSchema(BaseModel):
    id: int
    title: str = None


class TagOutSchema(TagSchema):
    type: int = None

    class Config:
        orm_mode = True






class TagRelationSchema(BaseModel):
    entity_type_id: int = None
    entity_id: int = None
    tag_id: int

class TagRelationInDbSchema(TagRelationSchema):
    pass