from pydantic import BaseModel

from schemas.file import FileOutSchema


class orderAttachmentSchema(BaseModel):
    text: str = None
    description: str = None

    class Config:
        orm_mode = True

class orderAttachmentOutSchema(orderAttachmentSchema):
    # id : int = None
    # order_id: int = None
    # photo : FileOutSchema = None
    # video : FileOutSchema = None
    pass

    class Config:
        orm_mode = True


class orderAttachmentInDbSchema(orderAttachmentSchema):
    order_id: int = None


class orderAttachmentInFormSchema(orderAttachmentSchema):
    photo : int = None
    video : int = None
