from typing import List, Optional

from pydantic import BaseModel


# Create the Pydantic models

class CategorySchema(BaseModel):
    title : str
    slug: str = None

    class Config:
        orm_mode = True

class CategoryOutSchema(CategorySchema):
    id: int


    class Config:
        orm_mode = True
