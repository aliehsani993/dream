from typing import Optional
from pydantic import BaseModel


class UserSchema(BaseModel):
    name: str = None

    class Config:
        orm_mode = True


class UserInDbSchema(UserSchema):
    password: str
    mobile: str


class UserInFormSchema(UserSchema):
    password: str
    mobile: str


class UserOutSchema(UserSchema):
    id : int
    email: Optional[str] = None

