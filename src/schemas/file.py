from pydantic import BaseModel



class FileSchema(BaseModel):
    format: str =None



class FileOutSchema(FileSchema):
    id: int

    class Config:
        orm_mode = True


#
class FileInDbSchema(FileSchema):
    id: int
    path: str = None
    title: str = None
    type: int = None
    format: str = None
    link: str = None

    class Config:
        orm_mode = True


