from fastapi import Depends, HTTPException
from dependencies.auth import get_current_user
from dependencies.db import get_db
from sqlalchemy.orm import Session
from repositories import channel as channel_repo


async def get_current_user_channel(user: get_current_user = Depends(get_current_user), db: Session = Depends(get_db)):
    return channel_repo.find_channel_by_user_id(db, user.id)
