from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, JWTError
from starlette import status
from configs import auth
from dependencies.db import get_db
from schema.token import TokenData
from repositories import user as user_repo
from sqlalchemy.orm import Session


async def get_current_user(token: str = Depends(OAuth2PasswordBearer(tokenUrl="token")), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, auth.SECRET_KEY, algorithms=[auth.ALGORITHM])
        mobile: str = payload.get("sub")
        if mobile is None:
            raise credentials_exception
        token_data = TokenData(mobile=mobile)
    except JWTError:
        raise credentials_exception
    user = user_repo.find_user_by_mobile(db, mobile=token_data.mobile)
    if user is None:
        raise credentials_exception
    return user
