from enum import Enum


class SettlementStatusEnum(Enum):
    NOTHING = 0

    @classmethod
    def _missing_(cls, value):
        return cls.NOTHING
