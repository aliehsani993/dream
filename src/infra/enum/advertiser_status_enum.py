from enum import Enum


class AdvertiserStatusEnum(Enum):
    CANCEL = 50
    NULL = None

    @classmethod
    def _missing_(cls, value):
        return cls.NULL


