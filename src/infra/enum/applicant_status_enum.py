from enum import Enum


class ApplicantStatusEnum(Enum):
    PAID = 40
    NOTHING = 0

    @classmethod
    def _missing_(cls, value):
        return cls.NOTHING
