import time
from urllib.request import Request
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers.auth import router as auth_router
from routers.user import router as user_router
from routers.channel import router as channel_router
from routers.plan import router as plan_router
from routers.order import router as order_router

dreamApp = FastAPI()

dreamApp.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

dreamApp.include_router(
    auth_router
)

dreamApp.include_router(
    user_router
)

dreamApp.include_router(
    channel_router
)
dreamApp.include_router(
    plan_router
)
dreamApp.include_router(
    order_router
)


@dreamApp.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response
